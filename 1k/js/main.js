$(function() {

    $(".container").hover(function () {
        $(this).addClass('animation-start');
    }, function () {
        $(this).removeClass('animation-start');
    });

    $("header").hover(function () {
        $('.animation', this).addClass('animation-start');
    }, function () {
        $('.animation', this).removeClass('animation-start');
    })

});