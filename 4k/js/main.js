$(function() {

    $(".container").hover(function () {
        $('body').addClass('animation-rows-start');
    }, function () {
        $('body').removeClass('animation-rows-start');
    });

    $("header").hover(function () {
        $('.animation', this).addClass('animation-start');
    }, function () {
        $('.animation', this).removeClass('animation-start');
    })

});